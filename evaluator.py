import sys
import json
import string

Q = {}
GT = {}

def score (list1,list2):
    list1 = [ x.encode("utf-8") for x in list1]
    list2 = [ x.encode("utf-8") for x in list2]
    intersection = len(set(list1) & set(list2))
    return intersection


if len(sys.argv) != 2:
    print("Usage: python evaluator.py outputfileofyourprogram")
    exit(1)

outputfile = sys.argv[1]

for l in open("queries.json"):
    dic = json.loads(l)
    Q[dic["internalID"]]= dic["participants"][0]["acheneID"]
    assert (len(dic["participants"])==1)

for l in open("groundtruth.json"):
    dic = json.loads(l)
    assert(len(dic["participants"])>1)
    GT[dic["acheneID"]]=dic["participants"]


correct = 0
predictions = 0
topredict = 0
good = 0
num = 0
for l in open(outputfile):
    num += 1
    fields = l.rstrip().split(",")
    internalID = fields[0]
    prediction = fields[1:]
    gt = GT["http://data.spaziodati.eu/resource/" + internalID[:-1]].copy()
    if len(gt)<= 1:
        print(len(gt))
        print(gt)
        print(internalID)
        exit(1)
    assert(len(gt)>1)
    gt.remove(Q[internalID])
    s = score(gt,prediction)
    if s >= 1 :
        good += 1
    correct += s
    predictions += len(prediction)
    topredict += len(gt)
#    print("%d %d %d" % (s,len(prediction),len(gt)))

print ("Precision :%.3f" % (correct/predictions))
print ("Recall : %.3f" % (correct/topredict))
print ("Good: %.3f" % (good/num))