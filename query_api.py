#!/usr/bin/env python
"""Query API.

Usage:
  query_api.py [--package PACKAGE] [--limit LIMIT] [--achene ACHENE]
  query_api.py (-h | --help)
  query_api.py --version

Options:
  -a --achene ACHENE    File with the list of achene IDs
                        [default: short_known_achenes.txt]
  -l --limit LIMIT      No. of records fetched for each request, max 50
                        [default: 50]
  -p --package PACKAGE  Package name PACKAGE [default: base]
  -h --help             Show this screen.
  --version             Show version.
"""

from docopt import docopt
import requests
import json

BASEURL = "https://api-u.spaziodati.eu/v2/companies"
TOKEN = "h-568ea38952724b939e5307b372461fdc"

if __name__ == '__main__':

    arguments = docopt(__doc__, version='Naval Fate 2.0')

    limit = int(arguments['--limit'])

    count = 0
    with open(arguments['--achene'], 'r') as f:

        achenes = set()
        for line in f:
            acheneId = line.strip().rsplit('/')[-1]

            achenes.add(acheneId)
            count = count + 1

            if count == limit:
                achene_list = list(achenes)
                ids_str = ','.join(achene_list)

                params = {'token': TOKEN,
                          'packages': arguments['--package'],
                          'ids': ids_str,
                          'limit': limit
                          }
                # curl -G "https://api-u.spaziodati.eu/v2/companies"
                #   -d "token=h-568ea38952724b939e5307b372461fdc"
                #   -d "ids=000069f7e916,,e1816a9f0495" -d "packages=base"
                r = requests.get(BASEURL, params=params)
                if r.ok:
                    print(json.dumps(r.json()))

                del achenes
                achenes = set()
                count = 0
