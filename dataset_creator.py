import json
import random

knowledgebase = open("oldstuff.json","w")
queries = open("queries.json","w")
groundtruth = open("groundtruth.json","w")
random.seed(123456789)

for l in open("for_unitn.json"):
    call = json.loads(l)
    isquery = False
    if "participants" in call:
        if 1 < len(call["participants"]) < 10:
            if random.random() < 0.1:
                achene = call["acheneID"].replace("http://data.spaziodati.eu/resource/","")
                isquery = True
                call2 = call.copy()
                gt = {}
                gt["participants"] = []
                gt["acheneID"] = call["acheneID"]
                i = 0
                for p in call["participants"]:
                    i += 1
                    call2["internalID"] = achene + str(i)
                    call2["participants"] = [p.copy()]
                    queries.write(json.dumps(call2) + "\n")
                    gt["participants"].append(p["acheneID"])
                groundtruth.write(json.dumps(gt) + "\n")


    if not isquery:
        knowledgebase.write(json.dumps(call) + "\n")
